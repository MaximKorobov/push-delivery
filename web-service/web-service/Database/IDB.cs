﻿using System;
using System.Collections.Generic;

namespace webservice
{
	public interface IDB
	{
		#region Initialization
		void Init ();
		void FillTestData ();
		#endregion

		#region Settings
		List<Setting> GetSettings ();
		#endregion

		#region Devices
		int Save (Device device);
		#endregion

		#region Applications
		Application GetApplication (String fullName);

		List<Application> GetApplications ();
		#endregion

		#region Campaigns
		Campaign GetCampaign (String name);

		List<Campaign> GetCampaigns ();
		CampaignStat GetCampaignStat (Campaign campaign);
		#endregion

		#region Application on device
		int Save (ApplicationOnDevice applicationOnDevice);
		ApplicationOnDevice GetApplicationOnDevice (String deviceId, String applicationFullName);
		ApplicationOnDevice GetApplicationOnDevice (String notificationId);

		int SetApplicationOnDeviceExpired (String notificationId);
		List<ApplicationOnDevice> GetApplicationsOnDevices (
			Application application, List<int> deviceStringIds, List<int> userIds, List<AttributeParameter> attributes
		);
		#endregion

		#region Attrbutes
		int Save (List<Attribute> attributes);
		#endregion

		#region Push notifications
		int SavePushNotification (PushNotification message, ApplicationOnDevice applicationOnDevice);

		int MovePushNotificationToArchive (int pushNotificationId, PushNotificationStatus status,
		                                          String statusText = "");
		int SetReadPushNotifications (List<int> pushNotifications);

		List<PushNotificationArchive> GetCampaignMessages (Campaign campaign);
		#endregion
	}
}

