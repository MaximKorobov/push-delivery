﻿using System;
using Dapper;

namespace webservice
{
	[Table("Queue")]
	public class PushNotificationQueue
	{
		[Key]
		public int QueueNotificationId { get; set; }

		public int PushNotificationId { get; set; }
		public int ApplicationOnDeviceId { get; set; }
		public int CampaignId { get; set; }

		public DateTime QueueDate { get; set; }

		public PushNotificationQueue()
		{
			QueueDate = DateTime.Now;
		}
	}
}