﻿using System;
using Dapper;

namespace webservice
{
	[Table("Devices")]
	public class Device
	{
		[Key]
		public int DeviceId { get; set; }

		public String DeviceStringId { get; set; }
		public String Platform { get; set; }

		public String Firmware { get; set; }
		public String Model { get; set; }
		public String Locale { get; set; }

		public DateTime RegistrationDate { get; set; }
		public DateTime LastUpdateDate { get; set; }
	}
}

