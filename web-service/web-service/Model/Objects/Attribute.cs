﻿using System;
using Dapper;

namespace webservice
{
	[Table("Attributes")]
	public class Attribute
	{
		[Key]
		public int AttributeId { get; set; }
		public int ApplicationOnDeviceId { get; set; }

		public int AttributeName { get; set; }
		public AttributeCondition AttributeCondition { get; set; }
		public int AttributeValue { get; set; }

		public Attribute(int applicationOnDeviceId, AttributeParameter ap)
		{
			this.ApplicationOnDeviceId = applicationOnDeviceId;

			this.AttributeName = ap.AttributeName;
			this.AttributeValue = ap.AttributeValue;
			this.AttributeCondition = ap.AttributeCondition;
		}
	}
}

