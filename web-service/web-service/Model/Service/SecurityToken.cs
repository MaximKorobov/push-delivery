﻿using System;
using Newtonsoft.Json;

namespace webservice
{
	public class SecurityToken
	{
		public String DeviceUid { get; set; }
		public String PushAddress { get; set; }
		public String AppPackage { get; set; }
		public String UserId { get; set; }

		public bool Equals(SecurityToken that)
		{
			var result =
				(this.AppPackage == that.AppPackage) &&
				(this.DeviceUid == that.DeviceUid) &&
				(this.PushAddress == that.PushAddress) &&
				(this.UserId == that.UserId);

			return result;
		}

		public SecurityToken ()
		{
		}

		public SecurityToken(String base64)
		{
			byte[] bytes = Convert.FromBase64String(base64);
			var jsonData = System.Text.Encoding.UTF8.GetString(bytes);
			var securityTokenData = JsonConvert.DeserializeObject<SecurityToken>(jsonData);

			DeviceUid = securityTokenData.DeviceUid;
			PushAddress = securityTokenData.PushAddress;
			AppPackage = securityTokenData.AppPackage;
			UserId = securityTokenData.UserId;
		}

		public String ToBase64()
		{
			var json = Newtonsoft.Json.JsonConvert.SerializeObject (this);

			byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(json);

			var base64 = Convert.ToBase64String(toEncodeAsBytes);

			return base64;
		}
	}
}

