﻿using System;
using Dapper;

namespace webservice
{
	[Table("Campaigns")]
	public class Campaign
	{
		[Key]
		public int CampaignId { get; set; }

		public String Name { get; set; }
		public DateTime CreationDate { get; set; }

		public int PlannedCount { get; set; }
	}
}

