﻿using System;
using Dapper;

namespace webservice
{
	[Table("Applications")]
	public class Application
	{
		[Key]
		public int ApplicationId { get; set; }

		public String FullName { get; set; }
		public Platform Platform { get; set; }
		public String Certificate { get; set; }
		public String CertificatePassword { get; set; }
		public String APIKey { get; set; }
		public String SenderId { get; set; }
	}
}

