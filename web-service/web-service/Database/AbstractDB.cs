﻿using System;
using System.Data;
using Dapper;
using System.Linq;

namespace webservice
{
	public abstract class AbstractDB
	{
        public abstract IDbConnection GetConnection();

		public void FillTestData()
        {
            using (var dbcon = GetConnection())
            {
                dbcon.Open();

                var name = "UseTestData";
                if (dbcon.Query<int>("select count(1) from Settings where SettingName=\"" + name + "\"")
                    .FirstOrDefault() == 0)
                {

                    // Applications
                    var androidApp = TestData.GetApplication(Platform.Android);
                    var testAndroidApplicationId = dbcon.Insert(androidApp);


                    // Android device
                    var androidDevice = new Device();
                    var testAndroidDeviceId = dbcon.Insert(androidDevice);

                    var androidApplicationOnDevice = new ApplicationOnDevice()
                        {
                            DeviceId = testAndroidDeviceId.GetValueOrDefault(0),
                            ApplicationId = testAndroidApplicationId.GetValueOrDefault(0),
                            NotificationId = TestData.AndroidPushAddress
                        };
                    dbcon.Insert(androidApplicationOnDevice);

                    for(int i = 0; i<1000; i++)
                    {
                        var fakeDevice = new Device();
                        var fakeTestAndroidDeviceId = dbcon.Insert(fakeDevice);
                        var fakeAndroidApplicationOnDevice = new ApplicationOnDevice()
                            {
                                DeviceId = fakeTestAndroidDeviceId.GetValueOrDefault(0),
                                ApplicationId = testAndroidApplicationId.GetValueOrDefault(0),
                                NotificationId = i.ToString() + "_test_" + TestData.AndroidPushAddress
                            };
                        dbcon.Insert(fakeAndroidApplicationOnDevice);
                    }

                    // iOS device
                    var iOSApp = TestData.GetApplication(Platform.iOS);
                    var testIOSApplicationId = dbcon.Insert(iOSApp);

                    var iOSDevice = new Device();
                    var testiOSDeviceId = dbcon.Insert(iOSDevice);

                    var iosApplicationOnDevice = new ApplicationOnDevice()
                        {
                            DeviceId = testiOSDeviceId.GetValueOrDefault(0),
                            ApplicationId = testIOSApplicationId.GetValueOrDefault(0),
                            NotificationId = TestData.iOSPushAddress
                        };
                    dbcon.Insert(iosApplicationOnDevice);

                    // Chrome device
                    var chromeApp = TestData.GetApplication(Platform.Chrome);
                    var testChromeApplicationId = dbcon.Insert(chromeApp);

                    var chromeDevice = new Device();
                    var chromeTestDeviceId = dbcon.Insert(chromeDevice);

                    var chromeApplicationOnDevice = new ApplicationOnDevice()
                        {
                            DeviceId = chromeTestDeviceId.GetValueOrDefault(0),
                            ApplicationId = testChromeApplicationId.GetValueOrDefault(0),
                            NotificationId = TestData.ChromePushAddress
                        };
                    dbcon.Insert(chromeApplicationOnDevice);

                    // Windows
                    var windowsApp = TestData.GetApplication(Platform.Windows);
                    dbcon.Insert(windowsApp);

                    // Firefox device
                    var firefoxApp = TestData.GetApplication(Platform.Firefox);
                    var testFirefoxApplicationId = dbcon.Insert(firefoxApp);

                    var firefoxDevice = new Device();
                    var firefoxTestDeviceId = dbcon.Insert(firefoxDevice);

                    var firefoxApplicationOnDevice = new ApplicationOnDevice()
                        {
                            DeviceId = firefoxTestDeviceId.GetValueOrDefault(0),
                            ApplicationId = testFirefoxApplicationId.GetValueOrDefault(0),
                            NotificationId = TestData.FirefoxPushAddress
                        };
                    dbcon.Insert(firefoxApplicationOnDevice);

                    // Save settings
                    dbcon.Insert(new Setting() { SettingName = name, SettingValue = "true" });
                }
            }
        }
	}
}

