﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Firefox;
using PushSharp.Google;

namespace webservice
{
    [WebService (Namespace = "https://tinkoff.ru/push/")]
    public class API : System.Web.Services.WebService
    {
        #region Info

        [WebMethod (Description = "Get current settings")]
        public List<Setting> GetSettings ()
        {
            Global.logger.Info ("Get settings");

            return Global.db.GetSettings ();
        }

        [WebMethod (Description = "Get all available applications")]
        public List<ApplicationShort> GetApplications ()
        {
            Global.logger.InfoFormat ("Get all applications");

            return Global.db.GetApplications ().Select (application => new ApplicationShort (application)).ToList ();
        }

        #endregion

        #region Campaign

        [WebMethod (Description = "Get campaigns")]
        public List<Campaign> GetCampaigns ()
        {
            Global.logger.InfoFormat ("Get campaigns");

            return Global.db.GetCampaigns ();
        }

        [WebMethod (Description = "Get campaign")]
        public List<Campaign> GetCampaign (String name)
        {
            Global.logger.InfoFormat ("Get campaign: {0}", name);

            return Global.db.GetCampaigns ();
        }

        [WebMethod (Description = "Get campaign stat")]
        public CampaignStatResult GetCampaignStat (String name)
        {
            Global.logger.InfoFormat ("Get campaign stat");

            if (String.IsNullOrEmpty (name)) {
                return new CampaignStatResult () {
                    ResultCode = ResultCode.WRONG_PARAMETERS,
                    Message = "Campaign name could not be null"
                };
            }

            var campaign = Global.db.GetCampaign (name);
            if (campaign == null)
                return new CampaignStatResult () {
                    ResultCode = ResultCode.NO_DATA_FOUND,
                    Message = String.Format ("Campaign with name {0} was not found")
                };

            var campaignStat = Global.db.GetCampaignStat (campaign);
            if (campaignStat == null)
                return new CampaignStatResult () {
                    ResultCode = ResultCode.INTERNAL_ERROR,
                    Message = "Stats: unable to get campaign stat"
                };

            var stat = "Stats:\n" + campaignStat.ToString ();
            Global.logger.InfoFormat (stat);
            return new CampaignStatResult () {
                ResultCode = ResultCode.OK,
                Message = stat,
                CampaignStat = campaignStat
            };
        }

        #endregion

        #region Other

        [WebMethod (Description = "Update iOS device badge")]
        public Result UpdateBadgeCount (String notificationId, int value, Boolean addToCurrentValue)
        {
            var applicationOnDevice = Global.db.GetApplicationOnDevice (notificationId);
            if (applicationOnDevice == null) {
                var errorText = String.Format (
                                    "Application on device not found: {0}", notificationId
                                );
                Global.logger.ErrorFormat (errorText);
                return new Result () { 
                    ResultCode = ResultCode.APPLICATION_ON_DEVICE_NOT_FOUND, 
                    Message = errorText
                };
            }

            var newValue = Math.Abs (value);
            if (addToCurrentValue) {
                newValue += applicationOnDevice.BadgeCount;
            }

            applicationOnDevice.BadgeCount = newValue;
            Global.db.Save (applicationOnDevice);

            return new Result ();
        }

        #endregion

        #region Delivery

        int QueueOnePushNotification (Application application, ApplicationOnDevice applicationOnDevice, String message)
        {
            var pushNotificationId = 0;

            var pushNotification = new PushNotification ();
            pushNotification.TitleText = message;

            switch (application.Platform) {
            case Platform.Android:
            case Platform.Chrome:
                /*
                var gcmNotification = new GcmNotification ();
				//notification.RegistrationIds = new List<String> () { TestData.AndroidPushAddress };
                gcmNotification.RegistrationIds = new List<String> () { applicationOnDevice.NotificationId };
                gcmNotification.Data = JObject.Parse ("{\"alert\":\"Hello World!\",\"badge\":" + 1 + ",\"sound\":\"sound.caf\",\"title\":\"now!\",\"message\":\"" + message + "\"}");

				// Save push message to DB
                pushNotification.Data = gcmNotification.Data.ToString ();
                pushNotificationId = Global.db.SavePushNotification (pushNotification, applicationOnDevice);
				// Queue push message with DB id
                gcmNotification.Tag = pushNotificationId;
                */

                pushNotification.Data = "{\"alert\":\"Hello World!\",\"badge\":" + 1 + ",\"sound\":\"sound.caf\",\"title\":\"now!\",\"message\":\"" + message + "\"}";
                //Global.gcmBrokers [application.FullName].QueueNotification (gcmNotification);
                Global.EnqueuePushNotification(pushNotification, application, applicationOnDevice);

                break;
            case Platform.iOS:
				// AppleNotificationPayload
				// Safari
				/*
				'aps' => array(
					'alert' => array(
						'title' => $this->title,
						'body' => $this->body,
					),
					'url-args' => array(
						$this->deepLink,
					),
				),*/
                var apsNotification = new ApnsNotification ();
				// Save push message to DB
                var payload = new {
                        aps = new {
                            alert = "alert:" + DateTime.Now.ToString (),
                            badge = applicationOnDevice.BadgeCount
                        },
                        payload = "payload"
                    };
                var stringPayload = JsonConvert.SerializeObject (payload);
                apsNotification.Payload = JObject.FromObject (payload);
                apsNotification.DeviceToken = applicationOnDevice.NotificationId;

                    // Save push message to DB
                pushNotification.Data = apsNotification.Payload.ToString ();
                pushNotificationId = Global.db.SavePushNotification (pushNotification, applicationOnDevice);

                    // Queue push message with DB id
                apsNotification.Tag = pushNotificationId;
                Global.apnsBrokers [application.FullName].QueueNotification (apsNotification);
                break;
            case Platform.Firefox:
                var firefoxNotification = new FirefoxNotification ();
                firefoxNotification.EndPointUrl = new Uri ("https://updates.push.services.mozilla.com/push/" + applicationOnDevice.NotificationId);
                Global.firefoxBrokers [application.FullName].QueueNotification (firefoxNotification);
                break;
            default:
                var errorMessage = String.Format ("Platform is not supported: {0}", application.Platform);
                var ex = new NotSupportedException (errorMessage);
                Global.logger.Fatal (ex);
                throw ex;
            }

            return pushNotificationId;
        }

        [WebMethod (Description = "Queue ONE push notification")]
        public Result QueueSimplePush (String message, String applicationFullName, String notificationId)
        {
            Global.logger.InfoFormat ("Queue push notification: {0}", applicationFullName);

            var application = Global.db.GetApplication (applicationFullName);
            if (application == null) {
                Global.logger.ErrorFormat ("Application not found: {0)", applicationFullName);
                return new Result () {
                    ResultCode = ResultCode.APPLICATION_NOT_FOUND,
                    Message = applicationFullName
                };
            }

            var applicationOnDevice = Global.db.GetApplicationOnDevice (notificationId);
            if (applicationOnDevice == null) {
                var errorText = String.Format (
                                    "Application on device not found: {0}", notificationId
                                );
                Global.logger.ErrorFormat (errorText);
                return new Result () { 
                    ResultCode = ResultCode.APPLICATION_ON_DEVICE_NOT_FOUND, 
                    Message = errorText
                };
            }

            var pushNotificationId = QueueOnePushNotification (application, applicationOnDevice, message);
            var resultMessage = String.Format ("Push message id = {0}", pushNotificationId);

            Global.logger.InfoFormat ("Push notification queued: {0}, {1}, {2}", 
                applicationFullName, applicationOnDevice.ApplicationOnDeviceId, pushNotificationId
            );

            return new Result () {
                ResultCode = ResultCode.OK,
                Message = resultMessage
            };
        }

        [WebMethod (Description = "Queue MANY push notifications")]
        public Result QueuePushNotifications (
            String applicationFullName, List<int> deviceStringIds, List<int> userIds, List<AttributeParameter> flags,
            String message, Boolean dryRun
        )
        {
            Global.logger.InfoFormat ("Queue push notification: {0}", applicationFullName);

            var application = Global.db.GetApplication (applicationFullName);
            if (application == null) {
                Global.logger.ErrorFormat ("Application not found: {0)", applicationFullName);
                return new Result () {
                    ResultCode = ResultCode.APPLICATION_NOT_FOUND,
                    Message = applicationFullName
                };
            }

            var applicationsOnDevices = Global.db.GetApplicationsOnDevices (
                                            application, deviceStringIds, userIds, flags
                                        );

            if (!dryRun) {
                Parallel.ForEach (applicationsOnDevices, (applicationOnDevice) => {
                    QueueOnePushNotification (application, applicationOnDevice, message);
                }
                );
            }

            return new Result () { 
                ResultCode = ResultCode.OK,
                Message = String.Format ("Messages to deliver: {0}", applicationsOnDevices.Count)
            };
        }

        #endregion
    }
}

