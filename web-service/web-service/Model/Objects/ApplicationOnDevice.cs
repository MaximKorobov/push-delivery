﻿using System;
using Dapper;

namespace webservice
{
	[Table("ApplicationsOnDevices")]
	public class ApplicationOnDevice
	{
		[Key]
		public int ApplicationOnDeviceId { get; set; }
		public int DeviceId { get; set; }
		public int ApplicationId { get; set; }

		public String NotificationId { get; set; }

        /// <summary>
        /// Gets or sets the badge count. iOS only
        /// </summary>
        /// <value>The badge count.</value>
        public int BadgeCount { get; set; }

		public String ApplicationVersion { get; set; }
		public String LastLogonUserId { get; set; }
		public DateTime? RegistrationDate { get; set; }
		public DateTime? LastUpdateDate { get; set; }
		public DateTime? NotificationIdExpirationDate { get; set; }
	}
}

