﻿using System;

namespace webservice
{
	public class Result
	{
		public Result()
		{
            Message = "";
            ResultCode = ResultCode.OK;
		}

		public ResultCode ResultCode { get; set; }
		public String Message { get; set; }
	}
}

