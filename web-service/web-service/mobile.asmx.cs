﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using Newtonsoft.Json;

namespace webservice
{
	[WebService(Namespace="https://tinkoff.ru/push/")]
	public class mobile : System.Web.Services.WebService
	{
		#region Registration
		[WebMethod(Description="Register new device or updates it's information using MFM's security token")]
		public Result RegisterDeviceMFM(String base64)
		{
			base64 = TestData.GetAndroidSecurityToken(); // TODO: temporary
			var securityTokenData = new SecurityToken (base64);

			var device = new Device() {
				DeviceStringId = securityTokenData.DeviceUid
			};
			return RegisterDevice(
				device, securityTokenData.AppPackage, securityTokenData.PushAddress, securityTokenData.UserId
			);
		}

		[WebMethod(Description="Register new device or updates it's information")]
		public Result RegisterDeviceMinimal(
			String deviceStringId, String applicationFullName, String notificationId, String userId)
		{
			var device = new Device () {
				DeviceStringId = deviceStringId
			};
			return RegisterDevice(device, applicationFullName, notificationId, userId);
		}

		[WebMethod(Description="Register new device or updates it's information")]
		public Result RegisterDevice(Device device, String applicationFullName, String notificationId, String userId)
		{
			Global.logger.InfoFormat(
				"Register device: \nDevice id: {0}\nApplication: {1}\nPushAddress: {2}\nUser: {3}", 
				new Object[] { device.DeviceStringId, applicationFullName, notificationId, userId
				});

			var application = Global.db.GetApplication (applicationFullName);
			if (application == null) {
				Global.logger.ErrorFormat ("Application not found: {0)", applicationFullName);
				return new Result () { ResultCode = ResultCode.APPLICATION_NOT_FOUND, Message = applicationFullName };
			}

			int deviceId = Global.db.Save (device);

			var applicationOnDevice = new ApplicationOnDevice () {
				ApplicationId = application.ApplicationId,
				DeviceId = deviceId,
				NotificationId = notificationId,
				LastLogonUserId = userId
			};
			int applicationOnDeviceId = Global.db.Save (applicationOnDevice);

			return new Result();
		}

		[WebMethod(Description="Set attributes for application on device")]
		public Result SetAttributes(String deviceId, String applicationFullName, List<AttributeParameter> attributes)
		{
			Global.logger.InfoFormat ("Set attributes");

			var applicationOnDevice = Global.db.GetApplicationOnDevice (deviceId, applicationFullName);
			if (applicationOnDevice == null) {
				Global.logger.ErrorFormat ("Application not found on device: {0), {1}", 
					deviceId, applicationFullName);
				return new Result () { ResultCode = ResultCode.APPLICATION_ON_DEVICE_NOT_FOUND, 
					Message = applicationFullName };
			}

			var attributesToSave = attributes.Select(attributeParameter => 
				new Attribute(applicationOnDevice.ApplicationOnDeviceId, attributeParameter)
			).ToList();

			Global.db.Save (attributesToSave);

			return new Result();
		}
		#endregion

		#region Delivery
		[WebMethod(Description="Set ONE push notification status as read")]
		public Result SetReadPushNotification(int pushNotification)
		{
			return SetReadPushNotifications (new List<int> () { pushNotification });
		}

		[WebMethod(Description="Set push notifications statuses as read")]
		public Result SetReadPushNotifications(List<int> pushNotifications)
		{
			Global.logger.InfoFormat("Set push notifications statuses as read: {0}", 
				String.Join(", ", pushNotifications));

			var affectedRows = Global.db.SetReadPushNotifications (pushNotifications);

			return new Result () {
				ResultCode = ResultCode.OK, 
				Message = "Affected push notifications: " + affectedRows.ToString() 
			};
		}
		#endregion
	}
}

