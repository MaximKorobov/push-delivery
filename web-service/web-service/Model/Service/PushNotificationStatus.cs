﻿using System;

namespace webservice
{
	public enum PushNotificationStatus
	{
		/// <summary>
		/// Push notification was registered in sustem
		/// </summary>
		REGISTER,
		/// <summary>
		/// Push notification is in queue
		/// </summary>
		INQUEUE,
		/// <summary>
		/// The error occured with push notification delivery
		/// </summary>
		ERROR,
		/// <summary>
		/// Push notification was send
		/// </summary>
		SENT,
		/// <summary>
		/// Push notification was deliver
		/// </summary>
		DELIVER,
		/// <summary>
		/// Push notification was read
		/// </summary>
		READ
	}
}

