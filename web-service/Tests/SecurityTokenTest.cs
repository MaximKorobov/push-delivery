﻿using NUnit.Framework;
using System;
using webservice;

namespace Tests
{
	[TestFixture (Description="Security token tests")]
	public class SecurityTokenTest
	{
		[Test (Description="Equals test")]
		public void EqualsTest ()
		{
			var securityToken1 = new SecurityToken();

			Assert.That (securityToken1.Equals (securityToken1));
		}

		[Test (Description="Android security token test")]
		public void Base64Test ()
		{
			var securityToken1 = new SecurityToken() {
				AppPackage = "1",
				DeviceUid = "2",
				PushAddress = "3",
				UserId = "4"
			};

			var base64 = securityToken1.ToBase64 ();
			var securityToken2 = new SecurityToken (base64);

			Assert.That (securityToken1.Equals (securityToken2));
		}

		[Test (Description="Android security token test")]
		public void AndroidSecurityTokenTest ()
		{
			var base64 = 
				"eyJhcHBQYWNrYWdlIjoiY29tLmlkYW1vYi50aW5rb2ZmLmFuZHJvaWQiLCJJTVNJIjoiMjUwMDI3NDE2Mzg" +
                "3Njk4Iiwic2NyZWVuUmVzb2x1dGlvblgiOiI1NDAiLCJkZXZpY2VVaWQiOiI5YzA4MDIzMTBkZDRjYjFiIiwiSU1FSSI6IjM1Mj" +
                "Y2MjA2MjIwNjk5MSIsImFwcFZlcnNpb24iOiI1MDYiLCJwdXNoQWRkcmVzcyI6ImZqZGpBV1V1R1pFOkFQQTkxYkdVNnpia3RFd" +
                "E1GbGJoVXdQVlRUbHdoWHB4YzlUeF9tTE1scHpDNXhfaDBzZmpxRFJ3dVFMd09xSm1kTWVIb1RfcTN4bmpOODZ1bmlBdENPNnRO" +
                "b0QwdnhaOE1CNWd2eU95YkdzbE9NX2NGNHp6ZjRoX3FiRjNOTk5ySWJJTlpyd0VvYzhYIiwiZGV2aWNlU2VyaWFsTnVtYmVyIjo" +
                "iTEdENjE4YzA2OWJjMDIiLCJ2ZXJzaW9uIjoiMi4xLjAtMjAxNTA4MzEtMjA0NiIsIm9zVmVyc2lvbiI6IjUuMC4yIiwiY2VsbE" +
                "lkIjoiNDAzODkxNjkiLCJkZXZpY2VNb2RlbCI6IkxHRSBMRy1ENjE4IiwicHJvdmlkZXJVaWQiOiJQSDVBY2tnek9rSnJNVmhvU" +
                "kdSclBrQkNiRGRlS1VGdVJWb29SRWxrWkN0Q2F5NVFmajRLIiwicm91dGVySXBBZGRyZXNzIjoiMTkyLjE2OC41LjE1MSIsImRl" +
                "dmljZU5hbWUiOiJqZW5raW5zIiwiaXBBZGRyZXNzIjoiZmU4MDo6ZmFhOTpkMGZmOmZlMzg6Y2NiZSV3bGFuMCIsInNjcmVlblJ" +
                "lc29sdXRpb25ZIjoiODg2IiwibG9jYWxlIjoicnUiLCJtZW1vcnlTaXplIjoiMjgxIiwidXNlclNlY3VyaXR5SGFzaCI6IjBqdE" +
                "FQM2RFWHV0QXVRRytNTFhtbFB4bG9EUT1cbiAgICAiLCJ0aW1lWm9uZVVUQ09mZnNldCI6IjEwODAwMDAwbXMiLCJvc05hbWUiO" +
                "iJBbmRyb2lkIE1TTTgyMjYiLCJtYWNBZGRyZXNzIjoiRjg6QTk6RDA6Mzg6Q0M6QkUiLCJyb3V0ZXJNYWNBZGRyZXNzIjoiZjg6" +
                "YTk6ZDA6Mzg6Y2M6YmUifQ==";

			var securityToken = new SecurityToken (base64);

            Assert.IsNotEmpty (securityToken.AppPackage);
            Assert.IsNotEmpty (securityToken.DeviceUid);
            Assert.IsNotEmpty (securityToken.PushAddress);
		}

		[Test (Description="iOS security token test")]
		public void iOSSecurityTokenTest ()
		{
			var base64 = 
				"ewogICJkZXZpY2VVaWQiIDogIjRmOGE0ZDYzZDQ1YTU4ZTE3YTU0Yzg3MTUxZGE0NTZiMWFhMWY4ODgiLAogICJvc05hbWUiIDo" +
				"gImlPUyIsCiAgInZlcnNpb24iIDogIjI3LTA2LTIwMTQiLAogICJvc1ZlcnNpb24iIDogIjkuMi4xIiwKICAibG9jYWxlIiA6IC" +
				"JydV9SVSIsCiAgInByb3ZpZGVyVWlkIiA6ICJQSDQ4SjNOVFRqVjFLQ2RiT21aVlJtSVwvVmlza2JDUXpmajRLIiwKICAiZGV2a" +
				"WNlTW9kZWwiIDogImlQaG9uZSIsCiAgImRldmljZU5hbWUiIDogImFWQm9iMjVsSU5DUTBMdlF0ZEM2MFlIUXNOQzkwTFRSZ0E9" +
				"PSIsCiAgInB1c2hBZGRyZXNzIiA6ICJXbnp3aFk0bE94MW4wdjlnVFZ5RUFFT1k5V3FOSU8zQitPWkZaSlZ6K0dvPSIsCiAgImF" +
				"wcFBhY2thZ2UiIDogInJ1LnRjc2JhbmsubW9iaWxlY2xpZW50IiwKICAidGltZVpvbmVVVENPZmZzZXQiIDogIjE0NDAwMDAwbX" +
				"MiLAogICJhcHBWZXJzaW9uIiA6ICIzLjEuMyIsCiAgInVzZXJTZWN1cml0eUhhc2giIDogImE2OTVmMzQ1ZDhhOTk1Nzg5ZmMxN" +
				"jZhNTFlNjE3MGExIgp9";

			var securityToken = new SecurityToken (base64);

            Assert.IsNotEmpty (securityToken.AppPackage);
            Assert.IsNotEmpty (securityToken.DeviceUid);
            Assert.IsNotEmpty (securityToken.PushAddress);
		}

		[Test (Description="Windows security token test")]
		public void WindowsSecurityTokenTest ()
		{
			var base64 = 
				"eyJ1c2VyU2VjdXJpdHlIYXNoIjoiYTVlN2NjNGFkYmIzNTI5MWNhZjY5ZmU5MjUxNDcyZWIiLCJkZXZpY2VVaWQiOiJjNmIwY2U" +
				"zNDJmNzJhOWZhMmM2YzMyYjQ5MmNjMzZiYiIsInB1c2hBZGRyZXNzIjoiYUhSMGNITTZMeTlvYXpJdWJtOTBhV1o1TG5kcGJtUn" +
				"ZkM011WTI5dEx6OTBiMnRsYmoxQmQxbEJRVUZDZWxFd01GZFdjbE5PVjFsWlJXbFFkelpuVEhGQldXdHBXRXdsTW1KWVpXcDNUb" +
				"VJqU0dOeVVHTXlWMUp6TlVkbmJYazNSVTVoT0ZWd2MyOWhZWHAzVW1zelNFbG5TbU14YWpCalFrZERZa2h6TmtFeWIxcDZibnBo" +
				"TkhOYVJESTRUMEptTmxrNFpUWlFabkZPTkdGd1dXNU9RMHR1VHlVeVlrdDVaamx4U1dSV2FqaHZKVE5rIiwib3NOYW1lIjoiV2l" +
				"uZG93c1Bob25lIiwib3NWZXJzaW9uIjoiOC4xIiwiZGV2aWNlTW9kZWwiOiJOT0tJQSIsImRldmljZU5hbWUiOiJXaW5kb3dzIH" +
				"Bob25lIiwicG9zaXRpb24iOiIiLCJtZW1vcnlTaXplIjoiIiwibG9jYWxlIjoicnUiLCJ0aW1lWm9uZVVUQ09mZnNldCI6IjEwO" +
				"DAwMDAwIiwiYXBwVmVyc2lvbiI6IjEuMC4wLjAiLCJhcHBQYWNrYWdlIjoiVENTQmFuay5DMkMiLCJ2ZXJzaW9uIjoiMjkuMDYu" +
				"MjAxNSAxNzo0ODoxMSIsInByb3ZpZGVyVWlkIjoiUEg0OEozTlRUa0E3WFdvMU5sTmZJamwrUGdvIn0=";

			var securityToken = new SecurityToken (base64);

            Assert.IsNotEmpty (securityToken.AppPackage);
            Assert.IsNotEmpty (securityToken.DeviceUid);
            Assert.IsNotEmpty (securityToken.PushAddress);
		}
	}
}

