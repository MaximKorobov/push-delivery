﻿using System;
using Dapper;

namespace webservice
{
	[Table("Settings")]
	public class Setting
	{
		[Key]
		public int SettingId { get; set; }

		public String SettingName { get; set; }
		public String SettingValue { get; set; }
	}
}

