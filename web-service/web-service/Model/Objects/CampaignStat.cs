﻿using System;

namespace webservice
{
	public class CampaignStat
	{
		public int PlannedCount { get; set; }
		public int SentCount { get; set; }
		public int DeliveredCount { get; set; }
		public int ReadCount { get; set; }

		public override String ToString()
		{
			String result = String.Empty;

			result += String.Format ("Planned count: {0}", this.PlannedCount);
			result += String.Format ("Sent count: {0}", this.SentCount);
			result += String.Format ("Delivered count: {0}", this.DeliveredCount);
			result += String.Format ("Read count: {0}", this.ReadCount);

			return result;
		}
	}
}

