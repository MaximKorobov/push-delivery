﻿using System;

namespace webservice
{
	public class ApplicationShort
	{
		public ApplicationShort ()
		{
		}

		public ApplicationShort (Application application)
		{
			this.ApplicationId = application.ApplicationId;

			this.FullName = application.FullName;
			this.Platform = application.Platform;
		}

		public int ApplicationId { get; set; }

		public String FullName { get; set; }
		public Platform Platform { get; set; }
	}
}

