﻿using System;
using System.IO;
using System.Text;
using log4net;
using MySql;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Firefox;
using PushSharp.Google;
using PushSharp.Windows;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace webservice
{
    class sender
    {
        static ILog logger;
        static long messagesCount;
        static string topic;
        static Application application;

        static IConnection connection;
        static IModel channel;

        static GcmServiceBroker gcmBroker;
        static ApnsServiceBroker apnsBroker;
        static WnsServiceBroker windowsBroker;
        static FirefoxServiceBroker firefoxBroker;

        public static void Main (string[] args)
        {
            try {
                InitLogging (); 
                InitParameters (args);
                InitQueue ();
                //InitBroker ();
            } catch (Exception ex) {
                String error = String.Format ("Error: {0}", ex);

                logger.ErrorFormat (error);
                Console.WriteLine (error);
            }

            WaitForEvents ();
        }

        static void InitLogging ()
        {
            try {
                var log4netConfigPath = "../../sender-logging.config";
                if (!String.IsNullOrEmpty (log4netConfigPath) && File.Exists (log4netConfigPath)) {
                    var log4netConfig = new FileInfo (log4netConfigPath);
                    if (log4netConfig != null) {
                        log4net.Config.XmlConfigurator.ConfigureAndWatch (log4netConfig);                            
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine ("Exception: " + ex);
            }
            Console.Clear ();
            logger = log4net.LogManager.GetLogger ("main");
        }

        static void InitParameters (string[] args)
        {
            var db = new MySQLDB (logger); 

            if (args.Length < 1) {
                logger.FatalFormat ("Usage: [application full name]");
                Environment.Exit (1);                
            }

            topic = args [0];
            logger.Info (topic);

            application = db.GetApplication (topic);
            if (application == null) {
                logger.FatalFormat ("Application not found: {0}", topic);
                Environment.Exit (1);
            }

            messagesCount = 0;
        }

        static void InitQueue ()
        {
            var factory = new ConnectionFactory () { 
                HostName = "localhost"
            };
            connection = factory.CreateConnection ();
            channel = connection.CreateModel ();

            channel.ExchangeDeclare ("topic_push_messages", "topic", true);
            var queueName = channel.QueueDeclare ().QueueName;
            channel.QueueBind (queueName, "topic_push_messages", topic);

            logger.Info ("Waiting for messages");

            var consumer = new EventingBasicConsumer (channel);
            consumer.Received += (model, ea) => {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString (body);
                var routingKey = ea.RoutingKey;
                logger.InfoFormat ("Message received '{0}':'{1}' bytes", routingKey, message.Length);

                using (var cm = JsonConvert.DeserializeObject<PushNotificationChannelMessage> (message)) {
                    var application = cm.Application;
                    var applicationOnDevice = cm.ApplicationOnDevice;
                    var pushNotification = cm.PushNotification;

                    switch (application.Platform) {
                    case Platform.Android:
                    case Platform.Chrome:
                        var gcmNotification = new GcmNotification () {
                            RegistrationIds = new List<String> () { applicationOnDevice.NotificationId },
                            Data = JObject.Parse (pushNotification.Data)
                        };
                        gcmBroker.QueueNotification (gcmNotification);
                        break;
                    case Platform.iOS:
                    case Platform.Windows:
                    case Platform.Firefox:
                    default:
                        var errorMessage = String.Format ("Platform is not supported: {0}", application.Platform);
                        var ex = new NotSupportedException (errorMessage);
                        logger.Fatal (ex);
                        throw ex;
                    }
                    messagesCount++;
                }
            };
            channel.BasicConsume (queueName, false, consumer);
        }

        static void InitBroker ()
        {
            var defaultChannelsCount = 5;

            switch (application.Platform) {
            case Platform.Android:
            case Platform.Chrome:
                var gcmConfiguration = new GcmConfiguration (
                                           application.SenderId, 
                                           application.APIKey,
                                           application.FullName
                                       );
                gcmBroker = new GcmServiceBroker (gcmConfiguration);
                gcmBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                gcmBroker.OnNotificationFailed += OnNotificationFailed;
                gcmBroker.ChangeScale (defaultChannelsCount);
                gcmBroker.Start ();

                if (application.Platform == Platform.Android) {
                    logger.InfoFormat ("Android service registered: {0}", application.FullName);
                } else {
                    logger.InfoFormat ("Chrome service registered: {0}", application.FullName);
                }
                break;
            case Platform.iOS:
                var iosConfiguration = new ApnsConfiguration (
                                           ApnsConfiguration.ApnsServerEnvironment.Sandbox, // TODO: create attribute
                                           application.Certificate.ToBytes (),
                                           application.CertificatePassword
                                       );
                apnsBroker = new ApnsServiceBroker (iosConfiguration);
                apnsBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                apnsBroker.OnNotificationFailed += OnNotificationFailed;
                apnsBroker.ChangeScale (defaultChannelsCount);                  
                apnsBroker.Start ();

                logger.InfoFormat ("iOS service registered: {0}", application.FullName);
                break;
            case Platform.Windows:
                var windowsConfiguration = new WnsConfiguration (
                                               application.FullName,
                                               application.APIKey,
                                               application.CertificatePassword
                                           );
                windowsBroker = new WnsServiceBroker (windowsConfiguration);
                windowsBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                windowsBroker.OnNotificationFailed += OnNotificationFailed;
                windowsBroker.ChangeScale (defaultChannelsCount);                  
                windowsBroker.Start ();

                logger.InfoFormat ("Windows service registered: {0}", application.FullName);
                break;
            case Platform.Firefox:
                var firefoxConfiguration = new FirefoxConfiguration () {
                    //  
                };
                firefoxBroker = new FirefoxServiceBroker (firefoxConfiguration);
                firefoxBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                firefoxBroker.OnNotificationFailed += OnNotificationFailed;
                firefoxBroker.ChangeScale (defaultChannelsCount);
                firefoxBroker.Start ();

                logger.InfoFormat ("Firefox service registered: {0}", application.FullName);
                break;
            default:
                var message = String.Format ("Platform is not supported: {0}", application.Platform);
                var ex = new NotSupportedException (message);
                logger.Fatal (ex);
                throw ex;
            }
        }

        static void OnNotificationSucceeded (INotification notification)
        {
            logger.InfoFormat ("Notification sent: {0}", notification);

            //MovePushNotificationToArchive(notification, PushNotificationStatus.SENT);
        }

        static void OnNotificationFailed (INotification notification, Exception ex)
        {
            logger.ErrorFormat ("Notification failed: {0}", notification.ToString ());
            logger.ErrorFormat ("Error: {0}", ex);

            //MovePushNotificationToArchive(notification, PushNotificationStatus.ERROR, ex.ToString());
            //SetApplicationOnDeviceExpired(notification, ex);
        }

        static void WaitForEvents ()
        {
            Console.WriteLine ("Press [enter] to exit.");

            String line;
            while ((line = Console.ReadLine ()) != "") {
                switch (line) {
                case "status":
                    Console.WriteLine ("Topic: {0}", topic);
                    Console.WriteLine ("Messages count: {0}", messagesCount);
                    break;
                case "exit":
                default:
                    goto Exit;
                }
            }

            Exit:;
            connection.Close ();
        }
    }
}