﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace webservice
{
    public class CacheDB: IDB
    {
        IDB db;

        // Inspired by this: http://www.allinsight.de/caching-objects-in-net-with-memorycache/

        //Get the default MemoryCache
        CacheItemPolicy defaultCachePolicy;
        ObjectCache applicationCache = MemoryCache.Default;
        ObjectCache applicationOnDeviceCache = MemoryCache.Default;

        public CacheDB(IDB db)
        {
            this.db = db;

            defaultCachePolicy = new CacheItemPolicy();
            defaultCachePolicy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(10);
        }

        #region IDB implementation

        public void Init()
        {
            db.Init();
        }

        public void FillTestData()
        {
            db.FillTestData();
        }

        public List<Setting> GetSettings()
        {
            return db.GetSettings();
        }

        public int Save(Device device)
        {
            return db.Save(device);
        }

        public Application GetApplication(string fullName)
        {
            var cacheApplication = (Application)applicationCache.Get(fullName);
            if (cacheApplication != null)
                return cacheApplication;

            var application = db.GetApplication(fullName);
            if (application != null)
            {
                applicationCache.Add(fullName, application, defaultCachePolicy);
            }

            return application;
        }

        public List<Application> GetApplications()
        {
            return db.GetApplications();
        }

        public Campaign GetCampaign(string name)
        {
            return db.GetCampaign(name);
        }

        public List<Campaign> GetCampaigns()
        {
            return db.GetCampaigns();
        }

        public CampaignStat GetCampaignStat(Campaign campaign)
        {
            return db.GetCampaignStat(campaign);
        }

        public int Save(ApplicationOnDevice applicationOnDevice)
        {
            return db.Save(applicationOnDevice);
        }

        public ApplicationOnDevice GetApplicationOnDevice(string deviceId, string applicationFullName)
        {
            var applicationOnDevice = db.GetApplicationOnDevice(deviceId, applicationFullName);
            return applicationOnDevice;
        }

        public ApplicationOnDevice GetApplicationOnDevice(string notificationId)
        {
            // Enable this to temporary speed uo (up to 30%) when testing with only one notification id.
            // Just like Volkswagen sometimes do. Lol :-P
            //
            //var cacheApplicationOnDevice = (ApplicationOnDevice)applicationOnDeviceCache.Get(notificationId);
            //if (cacheApplicationOnDevice != null)
            //    return cacheApplicationOnDevice;

            var applicationOnDevice = db.GetApplicationOnDevice(notificationId);
            //applicationOnDeviceCache.Add(notificationId, applicationOnDevice, defaultCachePolicy);

            return applicationOnDevice;
        }

        public int SetApplicationOnDeviceExpired(string notificationId)
        {
            return db.SetApplicationOnDeviceExpired(notificationId);
        }

        public List<ApplicationOnDevice> GetApplicationsOnDevices(Application application, List<int> deviceStringIds, List<int> userIds, List<AttributeParameter> attributes)
        {
            return db.GetApplicationsOnDevices(application, deviceStringIds, userIds, attributes);
        }

        public int Save(List<Attribute> attributes)
        {
            return db.Save(attributes);
        }

        public int SavePushNotification(PushNotification message, ApplicationOnDevice applicationOnDevice)
        {
            return db.SavePushNotification(message, applicationOnDevice);
        }

        public int MovePushNotificationToArchive(int pushNotificationId, PushNotificationStatus status, string statusText = "")
        {
            return db.MovePushNotificationToArchive(pushNotificationId, status, statusText);
        }

        public int SetReadPushNotifications(List<int> pushNotifications)
        {
            return db.SetReadPushNotifications(pushNotifications);
        }

        public List<PushNotificationArchive> GetCampaignMessages(Campaign campaign)
        {
            return GetCampaignMessages(campaign);
        }

        #endregion
    }
}

