﻿using System;

namespace webservice
{
	public class CampaignStatResult: Result
	{
		public CampaignStat CampaignStat { get; set; }
	}
}

