﻿using System;

namespace webservice
{
    public static class TestData
    {
        public const String AndroidApplicationName = "gcm.play.android.samples.com.gcmquickstart";
        public const String AndroidDeviceId = "test";
        public const String AndroidPushAddress = "f3zIlGfhVmY:APA91bFYOC86DKVgFM9OfPV5lhafDA4vwrJkGe23l65auQ0Cmcy0_FHMDR7mkrsB25x5LlYjjgvE78N9Wwr4uGCyOKFr2m5-2wsus6Xo71ra1nOxnK34txPOv4UjZVUAQd4Fu9z7IETg";

        public const String iOSApplicationName = "ru.tcsbank.TCSMT.qa2";
        public const String iOSDeviceId = "2639857e0d943feb0b9bcee4546660b1854c91b6";
        public const String iOSPushAddress = "K1sOln2nV8DFFzjtx\\/Fqy8vssEJamC\\/oJ1+OM0T2NSk=";

        public const String ChromeApplicationName = "testChromePage";
        public const String ChromePushAddress = "eltDDFLqtcs:APA91bEigsLjVykjo6HB1cGUzroIPzoNS7FzOVzYGwqkjhsME1Lsyzif5YVCNYH0gFn_IKPuq1MGiizo-X7VhJY8Giu6dgE8Set2QZJwkuZ42wbr3NNPXiKz2I_ISRs_B5WPc9ZKtoS2";

        public const String FirefoxApplicationName = "testFirefoxPage";
        public const String FirefoxPushAddress = "gAAAAABW4eCJYLOb07ENXQDLju9vcQ-xmj-RZxeUdiXwKGAauAeEvZaVJe2qsim7QnPUwD9vijrVje4Ru8kCZMwKIeg0ucTRpknz-KkhY1MqumPbwfWJ6D7WRXnAbLQpPCIf0YsOH0_b3CZaKES91QRHy0lYGxDNXBgcHkorc4qR7tS9atc1u7k=";

        /// <summary>
        /// Gets the android security token.
        /// </summary>
        /// <returns>The android security token.</returns>
        public static String GetAndroidSecurityToken()
        {
            var securityToken = new SecurityToken()
            {
                AppPackage = AndroidApplicationName,
                DeviceUid = AndroidDeviceId,
                PushAddress = AndroidPushAddress
            };
            var base64 = securityToken.ToBase64();

            return base64;
        }

        public static Application GetApplication(Platform platform)
        {
            switch (platform)
            {
                case Platform.Android:
                    var androidApp = new Application()
                    {
                        Platform = Platform.Android,
                        FullName = TestData.AndroidApplicationName,
                        SenderId = "393617546971",
                        APIKey = "AIzaSyA2Z0kmS6qP9vy10LO3-2B44si3gXUDl9g"
                    };
                    return androidApp;
                case Platform.Chrome:
                    var chromeApp = new Application()
                    {
                        Platform = Platform.Chrome,
                        FullName = TestData.ChromeApplicationName,
                        SenderId = "393617546971",
                        APIKey = "AIzaSyDk_qQGc172vU-icFNv6femkS0bU9PFBY4"
                    };
                    return chromeApp;
                case Platform.iOS:
				var appleApp = new Application () {
                        Platform = Platform.iOS,
					FullName = "test.ios",
					Certificate = "MIIC+DCCAeCgAwIBAgIQEcI4dFFEYoxLarzGW/7e0TANBgkqhkiG9w0BAQUFADAPMQ0wCwYDVQQDEwRIb" +
					"21lMCAXDTEzMTIxMzE4MDQ0NFoYDzIxMTMxMTE5MTgwNDQ0WjAPMQ0wCwYDVQQDEwRIb21lMIIBIjANBgkqhkiG9w0B" +
					"AQEFAAOCAQ8AMIIBCgKCAQEA8qoV39LxquWOFFpFX8zUIyU/JweHU+4kJz1Pso6K6Svpe3NpkrwzCFQzDAt146fzRS6" +
					"wI7JmoZjg5nLWTUZb3UPy1qYIhmhu1n1qKH6UFKSQg1KieoFq/9YhJ9Q/6/5OJfxh6l+uXFZ/7muB6BydW17cSPRkAa" +
					"/nXhrj0XqFIwhMJCiuo2pYx4lnVidegi+kWIVkXHH4qJBw68PHhtGdsxhKPTgMq0j4iqFZ2vucL3yosNgTPPiQahsDN" +
					"EfSx/s9xDIUPiSaI3E9utIs/HQOBsZ5og0z+OKh+KHq4xNoQ+mo2bnn8G6FtI5aY5kpnIvQmqfQUb6R7hz8yIoiMD9I" +
					"owIDAQABo04wTDAVBgNVHSUEDjAMBgorBgEEAYI3CgMEMCgGA1UdEQQhMB+gHQYKKwYBBAGCNxQCA6APDA1Ib21lQEh" +
					"vbWUtUEMAMAkGA1UdEwQCMAAwDQYJKoZIhvcNAQEFBQADggEBACNSbzmOE6XKJawf0mbuyoiWD5qRoSFg4riJIcp865" +
					"XohNIoSEKWBI/RblyFyoPzNp872nfOJRo8PyS3HwTfOk5NeE9qeDIL1cctXrf4mXRDJP9rCL1Lj5IEdYKNxy8fnQV2s" +
					"NJWzp1MWaV8JNH+BRToJXNw0KS9jHMHBrgGdF58cynt2mAP1HhaumUqQxDoVt6/XTUhJ7RDcFism0ivvyt7uMxHZh7G" +
					"Tbi9ommHs3z/eSyr7urevB5dTeRkL+M4EsqUJ2tHalSoXnjLd2EQsdyO+Pr6pwL1LtVorj/oa8Q9k+eKG/342xIjZPc" +
					"hBdsHQdkJaboELx5Hzcpe2SMKlwg=",
					CertificatePassword = "asd"
                    };
                    return appleApp;
                case Platform.Windows:
				var windowsApp = new Application () {
                        Platform = Platform.Windows,
                        FullName = "test.windows",
                        APIKey = "asd",
                        CertificatePassword = "asdd"
                    };
                    return windowsApp;
                case Platform.Firefox:
                    var firefoxApp = new Application()
                    {
                        Platform = Platform.Firefox,
                        FullName = TestData.FirefoxApplicationName
                    };
                    return firefoxApp;

                default:
                    throw new NotSupportedException("Platform is not supported");
            }
        }
    }
}

