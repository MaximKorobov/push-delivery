﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace webservice
{
    public static class Text
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if(enumerable == null)
                return true;

            return !enumerable.Any();
        }
    }
}

