﻿using System;
using System.Collections.Generic;
using System.Linq;
using PushSharp.Core;
using PushSharp.Google;

namespace webservice
{
	public static class Extensions
	{
		public static String ToString(this GcmNotification notification)
		{
			String result = String.Empty;
            result += String.Format ("Data: ", notification.Data);
            result += String.Format ("Ids: ", String.Join(", ", notification.Data));
			return result;
		}

		public static byte[] ToBytes(this String str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}
	}
}

