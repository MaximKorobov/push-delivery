﻿using System;

namespace webservice
{
    public class PushNotificationChannelMessage: IDisposable
    {
        public PushNotification PushNotification { get; set; }

        public Application Application { get; set; }

        public ApplicationOnDevice ApplicationOnDevice { get; set; }

        #region IDisposable implementation

        public void Dispose ()
        {
        }

        #endregion
    }
}

