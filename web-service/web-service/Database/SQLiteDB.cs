﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using Mono.Data.Sqlite;
using System.Collections.Generic;

namespace webservice
{
    public class SQLiteDB: AbstractDB, IDB
    {
        // SQLite has some concurrency problems
        // https://developer.xamarin.com/guides/cross-platform/application_fundamentals/data/part_2_configuration/
        object locker;
        IDbConnection dbcon;

        #region Initialization

        public SQLiteDB()
        {
            locker = new object();

            Dapper.SimpleCRUD.SetDialect(SimpleCRUD.Dialect.SQLite);

            const string connectionString = "URI=file:data.sqlite";
            dbcon = new SqliteConnection(connectionString);
            dbcon.Open();			
        }

        public override IDbConnection GetConnection()
        {
            return dbcon;
        }

        public void Dispose()
        {
            dbcon.Close();
            dbcon = null;
        }

        public void Init()
        {
            lock (locker)
            {
                dbcon.Open();

                // Settings
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Settings(" +
                    "SettingId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "SettingName TEXT," +
                    "SettingValue TEXT)"
                );

                if (dbcon.Query<int>("select count(1) from Settings").FirstOrDefault() > 0)
                {
                    dbcon.Close();
                    return;
                }

                dbcon.Execute("insert into Settings(SettingName, SettingValue) values(\"expirationTime\", \"86400\")");
                dbcon.Execute("insert into Settings(SettingName, SettingValue) values(\"queueSize\", \"100\")");

                // Registration
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Devices(" +
                    "DeviceId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "DeviceStringId TEXT," +
                    "Platform TEXT," +
                    "Firmware TEXT," +
                    "Model TEXT," +
                    "Locale TEXT," +
                    "RegistrationDate DATE," +
                    "LastUpdateDate DATE)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Applications(" +
                    "ApplicationId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "FullName TEXT," +
                    "Platform TEXT," +
                    "Certificate TEXT," +
                    "CertificatePassword TEXT," +
                    "APIKey TEXT," +
                    "SenderId TEXT)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS ApplicationsOnDevices(" +
                    "ApplicationOnDeviceId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "DeviceId INTEGER," +
                    "ApplicationId INTEGER," +
                    "ApplicationVersion TEXT," +
                    "LastLogonUserId TEXT," +
                    "RegistrationDate DATE," +
                    "LastUpdateDate DATE," +
                    "NotificationId TEXT," +
                    "BadgeCount INTEGER," +
                    "NotificationIdExpirationDate DATE," +
                    "FOREIGN KEY(DeviceId) REFERENCES Devices(DeviceId)," +
                    "FOREIGN KEY(ApplicationId) REFERENCES Applications(ApplicationId))"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Attributes(" +
                    "AttributeId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "ApplicationOnDeviceId INTEGER," +
                    "AttributeName TEXT," +
                    "AttributeType INTEGER," +
                    "AttributeValue TEXT," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId))"
                );

                // Queue
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Campaigns(" +
                    "CampaignId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "Name TEXT," +
                    "CreationDate DATE," +
                    "PlannedCount INTEGER)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS PushNotifications(" +
                    "PushNotificationId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "CreationDate DATE," +
                    "Data TEXT," +
                    "Sound TEXT," +
                    "BadgeCount INTEGER," +
                    "Attributes TEXT," +
                    "UserId TEXT," +
                    "TitleText TEXT," +
                    "Icon TEXT," +
                    "Tag TEXT," +
                    "BodyText TEXT)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Queue(" +
                    "QueueNotificationId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "PushNotificationId INTEGER," +
                    "ApplicationOnDeviceId INTEGER," +
                    "CampaignId INTEGER," +
                    "QueueDate DATE," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId)," +
                    "FOREIGN KEY(CampaignId) REFERENCES Campaigns(CampaignId)," +
                    "FOREIGN KEY(PushNotificationId) REFERENCES PushNotifications(PushNotificationId)" +
                    ")"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Archive(" +
                    "ArchiveNotificationId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "PushNotificationId INTEGER," +
                    "ApplicationOnDeviceId INTEGER," +
                    "CampaignId INTEGER," +
                    "Status TEXT," +
                    "StatusDate DATE," +
                    "StatusDescription TEXT," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId)," +
                    "FOREIGN KEY(CampaignId) REFERENCES Campaigns(CampaignId)," +
                    "FOREIGN KEY(PushNotificationId) REFERENCES PushNotifications(PushNotificationId)" +
                    ")"
                );
            }
        }

        #endregion

        #region Settings

        public List<Setting> GetSettings()
        {
            lock (locker)
            {
                var settings = dbcon.Query<Setting>("select * from Settings");
                return settings.ToList();
            }
        }

        #endregion

        #region Devices

        public int Save(Device device)
        {
            lock (locker)
            {
                device.LastUpdateDate = DateTime.Now;

                var oldDev = dbcon.GetList<Device>(new { DeviceStringId = device.DeviceStringId });
                if (oldDev.AsList().Count > 0)
                {
                    device.DeviceId = oldDev.AsList()[0].DeviceId;
                    return dbcon.Update(device);
                }
                else
                {
                    device.RegistrationDate = DateTime.Now;
                    return dbcon.Insert(device).Value;
                }
            }
        }

        #endregion

        #region Applications

        public Application GetApplication(String fullName)
        {
            lock (locker)
            {
                return dbcon.Query<Application>("select * from Applications where FullName = @fullName", 
                    new { fullName }
                ).AsList().FirstOrDefault();
            }
        }

        public List<Application> GetApplications()
        {
            lock (locker)
            {
                return dbcon.Query<Application>("select * from Applications").ToList();
            }
        }

        #endregion

        #region Campaigns

        public Campaign GetCampaign(String name)
        {
            lock (locker)
            {
                return dbcon.Query<Campaign>(
                    "select * from Campaigns where name = @name", new { name }					
                ).ToList()[0];
            }
        }

        public List<Campaign> GetCampaigns()
        {
            lock (locker)
            {
                return dbcon.Query<Campaign>("select * from Campaigns").ToList();
            }
        }

        public CampaignStat GetCampaignStat(Campaign campaign)
        {
            lock (locker)
            {
                var campaignMessages = GetCampaignMessages(campaign);

                var campaignStat = new CampaignStat()
                {
                    PlannedCount = campaign.PlannedCount,
                    SentCount =
						campaignMessages.Where(message => message.Status == PushNotificationStatus.SENT).Count(),
                    DeliveredCount = 
						campaignMessages.Where(message => message.Status == PushNotificationStatus.DELIVER).Count(),
                    ReadCount =
						campaignMessages.Where(message => message.Status == PushNotificationStatus.READ).Count(),
                };

                return campaignStat;
            }
        }

        #endregion

        #region Application on device

        public int Save(ApplicationOnDevice applicationOnDevice)
        {
            lock (locker)
            {
                applicationOnDevice.LastUpdateDate = DateTime.Now;

                var oldDev = dbcon.GetList<ApplicationOnDevice>(
                     new { DeviceId = applicationOnDevice.DeviceId, ApplicationId = applicationOnDevice.ApplicationId }
                 );
                if (oldDev.AsList().Count > 0)
                {
                    applicationOnDevice.ApplicationOnDeviceId = oldDev.AsList()[0].DeviceId;
                    return dbcon.Update(applicationOnDevice);
                }
                else
                {
                    applicationOnDevice.RegistrationDate = DateTime.Now;
                    return dbcon.Insert(applicationOnDevice).Value;
                }
            }
        }

        public ApplicationOnDevice GetApplicationOnDevice(String deviceId, String applicationFullName)
        {
            lock (locker)
            {
                var application = GetApplication(applicationFullName);

                var applicationsOnDevice = dbcon.GetList<ApplicationOnDevice>(
                                   new { DeviceId = deviceId, ApplicationId = application.ApplicationId }
                               );

                if (applicationsOnDevice.AsList().Count > 0)
                    return applicationsOnDevice.AsList()[0];
                return null;
            }
        }

        public ApplicationOnDevice GetApplicationOnDevice(String notificationId)
        {
            lock (locker)
            {
                var applicationsOnDevice = dbcon.GetList<ApplicationOnDevice>(
                                   new { NotificationId = notificationId }
                               );

                if (applicationsOnDevice.AsList().Count > 0)
                    return applicationsOnDevice.AsList()[0];
                return null;
            }
        }

        public int SetApplicationOnDeviceExpired(String notificationId)
        {
            lock (locker)
            {
                return dbcon.Execute("update ApplicationsOnDevices" +
                    " set NotificationIdExpirationDate = CURRENT_TIMESTAMP" +
                    " where notificationId = @notificationId",
                    new { notificationId }
                );
            }
        }

        public List<ApplicationOnDevice> GetApplicationsOnDevices(
            Application application, List<int> deviceStringIds, List<int> userIds, List<AttributeParameter> attributes
        )
        {
            lock (locker)
            {
                var tablesSQL = "select * from ApplicationsOnDevices aod";
                var conditionsSQL = " where aod.ApplicationId = " + application.ApplicationId +
                        " and aod.NotificationIdExpirationDate IS NULL";
                if (!deviceStringIds.IsNullOrEmpty())
                {
                    tablesSQL += " and aod.DeviceId in (" + String.Join(", ", deviceStringIds) + ")";
                }
                if (!userIds.IsNullOrEmpty())
                {
                    tablesSQL += " and aod.LastLogonUserId in (" + String.Join(", ", deviceStringIds) + ")";
                }
                if (!attributes.IsNullOrEmpty())
                {
                    tablesSQL += 
						" INNER JOIN Attributes atrs on aod.ApplicationOnDeviceId = atrs.ApplicationOnDeviceId";

                    foreach (var attribute in attributes)
                    {
                        switch (attribute.AttributeCondition)
                        {
                            case AttributeCondition.EQUALS:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value = \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                            case AttributeCondition.GREATER:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value > \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                            case AttributeCondition.LOWER:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value <> \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                        }
                    }
                }

                var selectSQL = tablesSQL + conditionsSQL;
                var applicationOnDevices = dbcon.Query<ApplicationOnDevice>(selectSQL).ToList();
                return applicationOnDevices;
            }
        }

        #endregion

        #region Attrbutes

        public int Save(List<Attribute> attributes)
        {
            lock (locker)
            {
                if (attributes.Count == 0)
                    return 0;

                // Delete old values
                foreach (var attribute in attributes)
                {
                    dbcon.Execute(
                        "delete from Attribute a where a.ApplicationOnDeviceId = @ApplicationOnDeviceId and " +
                        "AttributeName = @AttributeName", 
                        new { attribute.ApplicationOnDeviceId, attribute.AttributeName }
                    );
                }

                // Insert new values
                foreach (var attribute in attributes)
                {
                    dbcon.Insert(attribute);
                }
                return 0;
            }
        }

        #endregion

        #region Push notifications

        public int SavePushNotification(PushNotification message, ApplicationOnDevice applicationOnDevice)
        {
            lock (locker)
            {
                var pushNofificationId = dbcon.Insert(message).GetValueOrDefault(0);
                var queuePushNotification = new PushNotificationQueue()
                {
                    PushNotificationId = pushNofificationId,
                    ApplicationOnDeviceId = applicationOnDevice.ApplicationOnDeviceId,
                    //CampaignId = TODO,
                };
                var queuePushNotificationId = dbcon.Insert(queuePushNotification);

                return pushNofificationId;
            }
        }

        /// <summary>
        /// Moves the push notification to archive:
        /// 1. Get notification from queue
        /// 2. Save message to archive
        /// 3. Delete message from queue
        /// 4. Return archive message id
        /// </summary>
        /// <returns>The push notification id to archive.</returns>
        /// <param name="pushNotificationId">Push notification identifier.</param>
        public int MovePushNotificationToArchive(int pushNotificationId, PushNotificationStatus status,
                                           String statusText = "")
        {
            lock (locker)
            {
                var queuePushNotification = dbcon.Query<PushNotificationQueue>(
                                    "select * from Queue q where q.PushNotificationId = @PushNotificationId",
                                    new { pushNotificationId }
                                ).AsList()[0];

                var archivePushNotification = new PushNotificationArchive(queuePushNotification)
                {
                    Status = status,
                    StatusDate = DateTime.Now,
                    StatusDescription = statusText
                };
                var archivePushNotificationId = dbcon.Insert(archivePushNotification);
                var deleteCount = dbcon.Delete(queuePushNotification);
                if (deleteCount != 1)
                {
                    Global.logger.FatalFormat("Queued notification was not deleted: {0}", queuePushNotification);
                }

                return archivePushNotificationId.GetValueOrDefault(0);
            }
        }

        public int SetReadPushNotifications(List<int> pushNotifications)
        {
            lock (locker)
            {
                var pushNotificationIds = String.Join(", ", pushNotifications);

                if (pushNotificationIds.Count() == 0)
                    return 0;

                var status = PushNotificationStatus.DELIVER.ToString();

                return dbcon.Execute(
                    "update Archive set Status = @status where PushNotificationId in (@pushNotificationIds)",
                    new { status,  pushNotificationIds }
                );
            }
        }

        public List<PushNotificationArchive> GetCampaignMessages(Campaign campaign)
        {
            lock (locker)
            {
                return dbcon.Query<PushNotificationArchive>("select * from Archive where CampaignId = @campaignId",
                    new { campaign.CampaignId }
                ).ToList();
            }
        }

        #endregion
    }
}

