﻿using System;
using Dapper;

namespace webservice
{
	[Table("PushNotifications")]
	public class PushNotification
	{
		[Key]
		public int PushNotificationId { get; set; }

		// Common features
		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
		public String TitleText { get; set; }
		/// <summary>
		/// Gets or sets the content:
        /// - Safari: url-args
        /// - Firefox, Chrome: data field
		/// </summary>
		/// <value>The content.</value>
		public String Data { get; set; }
		public String Sound { get; set; }

		//
		// MOBILE
		//
		// Specific features
		/// <summary>
		/// Badge counter on icon. iOS only.
		/// </summary>
		/// <value>The badge.</value>
		public int BadgeCount { get; set; }

		/// <summary>
		/// TODO: improve according to Apple docs. iOS only
		/// </summary>
		/// <value>The attributes.</value>
		public String Attributes { get; set; }

		// Application features
		/// <summary>
		/// Gets or sets the user identifier.
		/// </summary>
		/// <value>The user identifier.</value>
		public String UserId { get; set; }

		//
		// WEB
        //
        // Safari:
        //      https://developer.apple.com/library/mac/documentation/NetworkingInternet/Conceptual/NotificationProgrammingGuideForWebsites/PushNotifications/PushNotifications.html
        //      http://samuli.hakoniemi.net/how-to-implement-safari-push-notifications-on-your-website/
        // Chrome, Firefox:
        //      https://developer.mozilla.org/en-US/docs/Web/API/Push_API/Using_the_Push_API
		//
		/// <summary>
		/// Gets or sets the body. Firefox, Chrome, Safari only
		/// </summary>
		/// <value>The body.</value>
		public String BodyText { get; set; }

        /// <summary>
        /// Gets or sets the icon. Firefox, Chrome only
        /// </summary>
        /// <value>The icon.</value>
        public String Icon { get; set; }

        /// <summary>
        /// Gets or sets the icon. Firefox, Chrome only
        /// </summary>
        /// <value>The icon.</value>
        public String Tag { get; set; }

		// System information
		/// <summary>
		/// Gets or sets the creation date. I.e. date when push notification was registered in system
		/// </summary>
		/// <value>The creation date.</value>
		public DateTime CreationDate { get; set; }

		public PushNotification()
		{
			CreationDate = DateTime.Now;
		}
	}
}

