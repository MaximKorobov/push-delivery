﻿using System;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;
using PushSharp.Core;

using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace webservice
{
	[WebService(Namespace="https://tinkoff.ru/push/")]
	public class admin : System.Web.Services.WebService
	{
		#region Actions
		[WebMethod(Description="Stop all services IMMEDIATELY")]
		public Result StopForce()
		{
			Global.logger.Info("Force stop");

            Global.Stop(true);

			return new Result () {
				Message = "OK",
				ResultCode = ResultCode.OK
			};
		}

		[WebMethod(Description="Stop all services after all queues were empty")]
		public Result StopAfterEmptyQueues()
		{
			Global.logger.Info("Stop after empty queues");

            Global.Stop(false);

			return new Result () {
				Message = "OK",
				ResultCode = ResultCode.OK
			};
		}
		#endregion

		#region Info
		[WebMethod(Description="Returns last events")]
		public List<String> GetLogsTail()
		{
			Global.logger.Info("Get log tail");

			var l = (Logger)Global.logger.Logger;
			var memoryAppender = l.GetAppender("MemoryAppender") as MemoryAppender;

			return memoryAppender.GetEvents().Select (x => 
				x.TimeStamp.ToString("yyyy.MM.dd HH:mm:ss") + ": " + x.RenderedMessage + "\n"
			).ToList ();
		}

		[WebMethod(Description="Returns running services and their statuses")]
        public Result GetStatus()
		{
			Global.logger.Info("Get status");

            return new Result () {
                    Message = "TODO: running apps and queue sizes",
                    ResultCode = ResultCode.OK
                };;
		}
		#endregion
	}
}

