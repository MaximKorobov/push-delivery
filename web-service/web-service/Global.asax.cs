using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using log4net;
using log4net.Repository.Hierarchy;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Firefox;
using PushSharp.Google;
using PushSharp.Windows;
using RabbitMQ.Client;
using Newtonsoft.Json;

namespace webservice
{
    public class Global : System.Web.HttpApplication
    {
        public static ILog logger;
        public static IDB db;

        public static Dictionary<String, GcmServiceBroker> gcmBrokers = new Dictionary<string, GcmServiceBroker> ();
        public static Dictionary<String, ApnsServiceBroker> apnsBrokers = new Dictionary<string, ApnsServiceBroker> ();
        public static Dictionary<String, WnsServiceBroker> wnsBrokers = new Dictionary<string, WnsServiceBroker> ();
        public static Dictionary<String, FirefoxServiceBroker> firefoxBrokers = new Dictionary<string, FirefoxServiceBroker> ();

        public static IConnection connection;
        public static Dictionary<String, IModel> channels = new Dictionary<string, IModel> ();

        #region Start and stop

        protected virtual void Application_Start (Object sender, EventArgs e)
        {
            initLogging ();
            initDatabase ();
            //initServices ();
            initQueues ();
        }

        void initLogging ()
        {
            try {
                var log4netConfigPath = Server.MapPath ("service-logging.config");
                if (!String.IsNullOrEmpty (log4netConfigPath) && File.Exists (log4netConfigPath)) {
                    var log4netConfig = new FileInfo (log4netConfigPath);
                    if (log4netConfig != null) {
                        log4net.Config.XmlConfigurator.ConfigureAndWatch (log4netConfig);                            
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine ("Exception: " + ex);
            }
            logger = log4net.LogManager.GetLogger ("main");

            var memoryAppender = new LimitedMemoryAppender ("MemoryAppender");
            var l = (Logger)logger.Logger;
            l.AddAppender (memoryAppender);
        }

        void initDatabase ()
        {
            //IDB realDB = new SQLiteDB ();
            var realDB = new MySQLDB (logger);
            db = new CacheDB (realDB);
            db.Init ();
            db.FillTestData ();
        }

        void initServices ()
        {
            logger.Info ("-------------------------------------------------------------------------------------------");
            logger.Info ("Init services");
            var defaultChannelsCount = 5;

            foreach (var application in db.GetApplications()) {
                switch (application.Platform) {
                case Platform.Android:
                case Platform.Chrome:
                    var gcmConfiguration = new GcmConfiguration (
                                                   application.SenderId, 
                                                   application.APIKey,
                                                   application.FullName
                                               );
                    var gcmBroker = new GcmServiceBroker (gcmConfiguration);
                    gcmBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                    gcmBroker.OnNotificationFailed += OnNotificationFailed;
                    gcmBroker.ChangeScale (defaultChannelsCount);
                    gcmBroker.Start ();

                    gcmBrokers.Add (application.FullName, gcmBroker);
                    if (application.Platform == Platform.Android) {
                        logger.InfoFormat ("Android service registered: {0}", application.FullName);
                    } else {
                        logger.InfoFormat ("Chrome service registered: {0}", application.FullName);
                    }
                    break;
                case Platform.iOS:
                    var iosConfiguration = new ApnsConfiguration (
                                                   ApnsConfiguration.ApnsServerEnvironment.Sandbox, // TODO: create attribute
                                                   application.Certificate.ToBytes (),
                                                   application.CertificatePassword
                                               );
                    var apnsBroker = new ApnsServiceBroker (iosConfiguration);
                    apnsBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                    apnsBroker.OnNotificationFailed += OnNotificationFailed;
                    apnsBroker.ChangeScale (defaultChannelsCount);                  
                    apnsBroker.Start ();

                    apnsBrokers.Add (application.FullName, apnsBroker);
                    logger.InfoFormat ("iOS service registered: {0}", application.FullName);
                    break;
                case Platform.Windows:
                    var windowsConfiguration = new WnsConfiguration (
                                                       application.FullName,
                                                       application.APIKey,
                                                       application.CertificatePassword
                                                   );
                    var windowsBroker = new WnsServiceBroker (windowsConfiguration);
                    windowsBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                    windowsBroker.OnNotificationFailed += OnNotificationFailed;
                    windowsBroker.ChangeScale (defaultChannelsCount);                  
                    windowsBroker.Start ();

                    wnsBrokers.Add (application.FullName, windowsBroker);
                    logger.InfoFormat ("Windows service registered: {0}", application.FullName);
                    break;
                case Platform.Firefox:
                    var firefoxConfiguration = new FirefoxConfiguration () {
                        //  
                    };
                    var firefoxBroker = new FirefoxServiceBroker (firefoxConfiguration);
                    firefoxBroker.OnNotificationSucceeded += OnNotificationSucceeded;
                    firefoxBroker.OnNotificationFailed += OnNotificationFailed;
                    firefoxBroker.ChangeScale (defaultChannelsCount);
                    firefoxBroker.Start ();

                    firefoxBrokers.Add (application.FullName, firefoxBroker);
                    logger.InfoFormat ("Firefox service registered: {0}", application.FullName);
                    break;
                default:
                    var message = String.Format ("Platform is not supported: {0}", application.Platform);
                    var ex = new NotSupportedException (message);
                    logger.Fatal (ex);
                    throw ex;
                }
            }
        }

        static void initQueues ()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            connection = factory.CreateConnection ();

            foreach (var application in db.GetApplications()) {
                switch (application.Platform) {
                case Platform.Android:
                case Platform.Chrome:
                case Platform.iOS:
                case Platform.Windows:
                case Platform.Firefox:
                    var topic = application.FullName;
                    var channel = connection.CreateModel ();
                    channel.ExchangeDeclare ("topic_push_messages", "topic", true);
                    var queueName = channel.QueueDeclare ().QueueName;
                    channel.QueueBind (queueName, "topic_push_messages", topic);
                    channels.Add (topic, channel);
                    break;
                default:
                    var message = String.Format ("Platform is not supported: {0}", application.Platform);
                    var ex = new NotSupportedException (message);
                    logger.Fatal (ex);
                    throw ex;
                }
            }
        }

        public static void EnqueuePushNotification (PushNotification pushNotification, Application application, 
            ApplicationOnDevice applicationOnDevice)
        {
            var topic = application.FullName;
            var channel = channels[topic];

            var pushNotificationChannelMessage = new PushNotificationChannelMessage () {
                PushNotification = pushNotification,
                Application = application,
                ApplicationOnDevice = applicationOnDevice
            };
            var message = JsonConvert.SerializeObject (pushNotificationChannelMessage);;
            var body = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish("topic_push_messages", topic, null, body);
        }

        void OnNotificationSucceeded (INotification notification)
        {
            logger.InfoFormat ("Notification sent: {0}", notification);

            MovePushNotificationToArchive (notification, PushNotificationStatus.SENT);
        }

        void OnNotificationFailed (INotification notification, Exception ex)
        {
            logger.ErrorFormat ("Notification failed: {0}", notification.ToString ());
            logger.ErrorFormat ("Error: {0}", ex);

            MovePushNotificationToArchive (notification, PushNotificationStatus.ERROR, ex.ToString ());
            SetApplicationOnDeviceExpired (notification, ex);
        }

        void MovePushNotificationToArchive (INotification notification, PushNotificationStatus status,
                                           String statusText = ""
        )
        {
            var pushNotificationId = Convert.ToInt32 (notification.Tag);
            var archivePushNotificationId = db.MovePushNotificationToArchive (
                                                pushNotificationId, status, statusText
                                            );
            if (archivePushNotificationId != 0) {
                var message = 
                    String.Format ("Push notification moved to archive: id {0}, archive id {1}, status {2}", 
                        pushNotificationId, archivePushNotificationId, status);
                if (!statusText.IsNullOrEmpty ())
                    message += String.Format (", status text {0}", statusText);
                logger.Info (message);
            }
        }

        void SetApplicationOnDeviceExpired (INotification notification, Exception ex)
        {
            var gcmErrorMessage = ex as GcmNotificationException;
            var gcmNotification = notification as GcmNotification;
            if ((gcmErrorMessage != null) && (gcmNotification != null)) {
                foreach (var registrationId in gcmNotification.RegistrationIds) {
                    var id = db.SetApplicationOnDeviceExpired (registrationId);
                    switch (gcmErrorMessage.Message) {
                    case "MismatchSenderId": // Wrong sender id
                    case "InvalidRegistration": // Invalid registration id - wrong
                        if (id != 0)
                            logger.ErrorFormat ("Notification id set as expired: {0}", registrationId);
                        else
                            logger.ErrorFormat ("Notification sent to unknown id: {0}", registrationId);
                        break;
                    default:
                        throw new NotSupportedException ("TODO");
                    }
                    ;
                }
            }
        }

        protected virtual void Application_Error (Object sender, EventArgs e)
        {
            logger.ErrorFormat ("Unhandled exception: {0}", e);
        }

        protected virtual void Application_End (Object sender, EventArgs e)
        {
            Stop (true);
        }

        public static void Stop (bool immediately)
        {
            // Well, PushSharp needs actually Pause() method.
            // Otherwise some push-messages in queue could not be updated their statuses if they was send
            // just before stop command called
            //				var message = (service as GcmPushService).
            //				var settings = service.ChannelSettings ();

            // TODO: save queue. Hm. Where is get queued messages method?

            // IDEA: use small batch (even more = setting) to avoid push-notifications loss on stop

            foreach (KeyValuePair<String, GcmServiceBroker> gcmBroker in gcmBrokers) {
                var service = gcmBroker.Value;
                service.Stop (immediately);
            }

            foreach (KeyValuePair<String, ApnsServiceBroker> apnsBroker in apnsBrokers) {
                var service = apnsBroker.Value;
                service.Stop (immediately);
            }

            foreach (KeyValuePair<String, WnsServiceBroker> wnsBroker in wnsBrokers) {
                var service = wnsBroker.Value;
                service.Stop (immediately);
            }

            foreach (KeyValuePair<String, FirefoxServiceBroker> firefoxBroker in firefoxBrokers) {
                var service = firefoxBroker.Value;
                service.Stop (immediately);
            }
        }

        #endregion

        #region Not used

        protected virtual void Session_Start (Object sender, EventArgs e)
        {
        }

        protected virtual void Application_BeginRequest (Object sender, EventArgs e)
        {
        }

        protected virtual void Application_EndRequest (Object sender, EventArgs e)
        {
        }

        protected virtual void Application_AuthenticateRequest (Object sender, EventArgs e)
        {
        }

        protected virtual void Session_End (Object sender, EventArgs e)
        {

        }

        #endregion
    }
}

