﻿using System;

namespace webservice
{
	public class AttributeParameter
	{
		public int AttributeName { get; set; }
		public AttributeCondition AttributeCondition { get; set; }
		public int AttributeValue { get; set; }
	}
}

