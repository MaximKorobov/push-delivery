﻿using System;
using Dapper;

namespace webservice
{
	[Table("Archive")]
	public class PushNotificationArchive
	{
		[Key]
		public int ArchiveNotificationId { get; set; }

		public int ApplicationOnDeviceId { get; set; }
		public int PushNotificationId { get; set; }
		public int CampaignId { get; set; }

		public PushNotificationStatus Status { get; set; }
		public DateTime StatusDate { get; set; }
		public String StatusDescription { get; set; }

		public PushNotificationArchive(PushNotificationQueue pushNotificationQueue)
		{
			this.ApplicationOnDeviceId = pushNotificationQueue.ApplicationOnDeviceId;
			this.PushNotificationId = pushNotificationQueue.PushNotificationId;
			this.CampaignId = pushNotificationQueue.CampaignId;
		}
	}
}

