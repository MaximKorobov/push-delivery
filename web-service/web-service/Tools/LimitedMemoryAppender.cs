﻿using System;
using log4net.Appender;
using log4net.Core;

namespace webservice
{
	public class LimitedMemoryAppender : MemoryAppender
	{
		public LimitedMemoryAppender (String name)
		{
			Name = name;
		}

		override protected void Append(LoggingEvent loggingEvent) 
		{
			base.Append(loggingEvent);
			if (m_eventsList.Count > 10)
			{
				m_eventsList.RemoveAt(0);
			}
		} 
	}
}

