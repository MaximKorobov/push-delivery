﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using log4net;
using MySql.Data.MySqlClient;

namespace webservice
{
    public class MySQLDB: AbstractDB, IDB
    {
        String connectionString;
        ILog logger;
        // Where is connection pool?
        // ADO.NET have build in connection pool
        // http://stackoverflow.com/questions/9705637/executereader-requires-an-open-and-available-connection-the-connections-curren/9707060#9707060

        #region Initialization

        public MySQLDB(ILog logger)
        {
            this.logger = logger;

            Dapper.SimpleCRUD.SetDialect(SimpleCRUD.Dialect.MySQL);

            connectionString =
				"Server=localhost;" +
            "Database=push;" +
            "User ID=root;" +
            "Password=root;" +
            "Pooling=false";
        }

        public override IDbConnection GetConnection()
        {
            var dbcon = new MySqlConnection(connectionString);
            return dbcon;
        }

        public void Init()
        {
            using (var dbcon = GetConnection())
            {
                dbcon.Open();

                // Settings
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Settings(" +
                    "SettingId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "SettingName TEXT," +
                    "SettingValue TEXT)"
                );

                if (dbcon.Query<int>("select count(1) from Settings").FirstOrDefault() > 0)
                {
                    dbcon.Close();
                    return;
                }

                // Registration
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Devices(" +
                    "DeviceId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "DeviceStringId TEXT," +
                    "Platform TEXT," +
                    "Firmware TEXT," +
                    "Model TEXT," +
                    "Locale TEXT," +
                    "RegistrationDate DATE," +
                    "LastUpdateDate DATE)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Applications(" +
                    "ApplicationId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "FullName TEXT," +
                    "Platform TEXT," +
                    "Certificate TEXT," +
                    "CertificatePassword TEXT," +
                    "APIKey TEXT," +
                    "SenderId TEXT)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS ApplicationsOnDevices(" +
                    "ApplicationOnDeviceId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "DeviceId INTEGER," +
                    "ApplicationId INTEGER," +
                    "ApplicationVersion TEXT," +
                    "LastLogonUserId TEXT," +
                    "RegistrationDate DATE NULL," +
                    "LastUpdateDate DATE NULL," +
                    "NotificationId TEXT NOT NULL," +
                    "BadgeCount INTEGER," +
                    "NotificationIdExpirationDate DATE NULL," +
                    "FOREIGN KEY(DeviceId) REFERENCES Devices(DeviceId)," +
                    "FOREIGN KEY(ApplicationId) REFERENCES Applications(ApplicationId))"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Attributes(" +
                    "AttributeId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "ApplicationOnDeviceId INTEGER," +
                    "AttributeName TEXT," +
                    "AttributeType INTEGER," +
                    "AttributeValue TEXT," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId))"
                );

                // Queue
                dbcon.Execute("CREATE TABLE IF NOT EXISTS Campaigns(" +
                    "CampaignId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "Name TEXT," +
                    "CreationDate DATE," +
                    "PlannedCount INTEGER)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS PushNotifications(" +
                    "PushNotificationId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "CreationDate DATE," +
                    "Data TEXT," +
                    "Sound TEXT," +
                    "BadgeCount INTEGER," +
                    "Attributes TEXT," +
                    "UserId TEXT," +
                    "TitleText TEXT," +
                    "Icon TEXT," +
                    "Tag TEXT," +
                    "BodyText TEXT)"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Queue(" +
                    "QueueNotificationId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "PushNotificationId INTEGER," +
                    "ApplicationOnDeviceId INTEGER," +
                    "CampaignId INTEGER," +
                    "QueueDate DATE," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId)," +
                    // TODO: temporary
                    //"FOREIGN KEY(CampaignId) REFERENCES Campaigns(CampaignId)," +
                    "FOREIGN KEY(PushNotificationId) REFERENCES PushNotifications(PushNotificationId)" +
                    ")"
                );

                dbcon.Execute("CREATE TABLE IF NOT EXISTS Archive(" +
                    "ArchiveNotificationId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                    "PushNotificationId INTEGER," +
                    "ApplicationOnDeviceId INTEGER," +
                    "CampaignId INTEGER," +
                    "Status TEXT," +
                    "StatusDate DATE," +
                    "StatusDescription TEXT," +
                    "FOREIGN KEY(ApplicationOnDeviceId) REFERENCES ApplicationsOnDevices(ApplicationOnDeviceId)," +
                    //"FOREIGN KEY(CampaignId) REFERENCES Campaigns(CampaignId)," +
                    "FOREIGN KEY(PushNotificationId) REFERENCES PushNotifications(PushNotificationId)" +
                    ")"
                );

                // INDEXES
                dbcon.Execute("ALTER TABLE applicationsondevices ADD unique INDEX notificationid(notificationid(200))");
                dbcon.Execute("ALTER TABLE queue ADD unique INDEX pushnotificationid(pushnotificationid)");
                dbcon.Execute("ALTER TABLE archive ADD unique INDEX pushnotificationid(pushnotificationid)");

                dbcon.Execute("insert into Settings(SettingName, SettingValue) values(\"expirationTime\", \"86400\")");
                dbcon.Execute("insert into Settings(SettingName, SettingValue) values(\"queueSize\", \"100\")");

                dbcon.Close();
            }
        }

        #endregion

        #region Settings

        public List<Setting> GetSettings()
        {
            using (var dbcon = GetConnection())
            {
                var settings = dbcon.Query<Setting>("select * from Settings");
                return settings.ToList();
            }
        }

        #endregion

        #region Devices

        public int Save(Device device)
        {
            device.LastUpdateDate = DateTime.Now;

            using (var dbcon = GetConnection())
            {
                var oldDev = dbcon.GetList<Device>(new { DeviceStringId = device.DeviceStringId }).ToList();
                if (oldDev.Count > 0)
                {
                    device.DeviceId = oldDev[0].DeviceId;
                    return dbcon.Update(device);
                }
                else
                {
                    device.RegistrationDate = DateTime.Now;
                    return dbcon.Insert(device).Value;
                }
            }
        }

        #endregion

        #region Applications

        public Application GetApplication(String fullName)
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Query<Application>("select * from Applications where FullName = @fullName", 
                    new { fullName }
                ).ToList().FirstOrDefault();
            }
        }

        public List<Application> GetApplications()
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Query<Application>("select * from Applications").ToList();
            }
        }

        #endregion

        #region Campaigns

        public Campaign GetCampaign(String name)
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Query<Campaign>(
                    "select * from Campaigns where name = @name", new { name }					
                ).ToList()[0];
            }
        }

        public List<Campaign> GetCampaigns()
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Query<Campaign>("select * from Campaigns").ToList();
            }
        }

        public CampaignStat GetCampaignStat(Campaign campaign)
        {
            using (var dbcon = GetConnection())
            {
                var campaignMessages = GetCampaignMessages(campaign);

                var campaignStat = new CampaignStat()
                {
                    PlannedCount = campaign.PlannedCount,
                    SentCount =
						campaignMessages.Where(message => message.Status == PushNotificationStatus.SENT).Count(),
                    DeliveredCount = 
						campaignMessages.Where(message => message.Status == PushNotificationStatus.DELIVER).Count(),
                    ReadCount =
						campaignMessages.Where(message => message.Status == PushNotificationStatus.READ).Count(),
                };

                return campaignStat;
            }
        }

        #endregion

        #region Application on device

        public int Save(ApplicationOnDevice applicationOnDevice)
        {
            using (var dbcon = GetConnection())
            {
                applicationOnDevice.LastUpdateDate = DateTime.Now;

                var oldDev = dbcon.GetList<ApplicationOnDevice>(new { 
				DeviceId = applicationOnDevice.DeviceId,
				ApplicationId = applicationOnDevice.ApplicationId
			}).ToList();
                if (oldDev.Count > 0)
                {
                    applicationOnDevice.ApplicationOnDeviceId = oldDev[0].DeviceId;
                    return dbcon.Update(applicationOnDevice);
                }
                else
                {
                    applicationOnDevice.RegistrationDate = DateTime.Now;
                    return dbcon.Insert(applicationOnDevice).Value;
                }
            }
        }

        public ApplicationOnDevice GetApplicationOnDevice(String deviceId, String applicationFullName)
        {
            using (var dbcon = GetConnection())
            {
                var application = GetApplication(applicationFullName);

                var applicationsOnDevice = dbcon.GetList<ApplicationOnDevice>(
                                               new { DeviceId = deviceId, ApplicationId = application.ApplicationId }
                                           ).ToList();
                if (applicationsOnDevice.Count > 0)
                    return applicationsOnDevice[0];
                return null;
            }
        }

        public ApplicationOnDevice GetApplicationOnDevice(String notificationId)
        {
            using (var dbcon = GetConnection())
            {
                var applicationsOnDevice = dbcon.GetList<ApplicationOnDevice>(
                                               new { NotificationId = notificationId }
                                           ).ToList();

                if (applicationsOnDevice.Count > 0)
                    return applicationsOnDevice[0];
                return null;
            }
        }

        public int SetApplicationOnDeviceExpired(String notificationId)
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Execute("update ApplicationsOnDevices" +
                    " set NotificationIdExpirationDate = CURRENT_TIMESTAMP" +
                    " where notificationId = @notificationId",
                    new { notificationId }
                );
            }
        }

        public List<ApplicationOnDevice> GetApplicationsOnDevices(
            Application application, List<int> deviceStringIds, List<int> userIds, List<AttributeParameter> attributes
        )
        {
            using (var dbcon = GetConnection())
            {
                var tablesSQL = "select * from ApplicationsOnDevices aod";
                var conditionsSQL = " where aod.ApplicationId = " + application.ApplicationId +
                                    " and aod.NotificationIdExpirationDate IS NULL";
                if (!deviceStringIds.IsNullOrEmpty())
                {
                    tablesSQL += " and aod.DeviceId in (" + String.Join(", ", deviceStringIds) + ")";
                }
                if (!userIds.IsNullOrEmpty())
                {
                    tablesSQL += " and aod.LastLogonUserId in (" + String.Join(", ", deviceStringIds) + ")";
                }
                if (!attributes.IsNullOrEmpty())
                {
                    tablesSQL += 
						" INNER JOIN Attributes atrs on aod.ApplicationOnDeviceId = atrs.ApplicationOnDeviceId";

                    foreach (var attribute in attributes)
                    {
                        switch (attribute.AttributeCondition)
                        {
                            case AttributeCondition.EQUALS:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value = \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                            case AttributeCondition.GREATER:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value > \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                            case AttributeCondition.LOWER:
                                conditionsSQL += String.Format(" and atrs.name = \"{0}\" and atrs.value <> \"{1}\"",
                                    attribute.AttributeName, attribute.AttributeValue);
                                break;
                        }
                    }
                }

                var selectSQL = tablesSQL + conditionsSQL;
                var applicationOnDevices = dbcon.Query<ApplicationOnDevice>(selectSQL).ToList();
                return applicationOnDevices;
            }
        }

        #endregion

        #region Attrbutes

        public int Save(List<Attribute> attributes)
        {
            using (var dbcon = GetConnection())
            {
                if (attributes.Count == 0)
                    return 0;

                // Delete old values
                foreach (var attribute in attributes)
                {
                    dbcon.Execute(
                        "delete from Attribute a where a.ApplicationOnDeviceId = @ApplicationOnDeviceId and " +
                        "AttributeName = @AttributeName", 
                        new { attribute.ApplicationOnDeviceId, attribute.AttributeName }
                    );
                }

                // Insert new values
                foreach (var attribute in attributes)
                {
                    dbcon.Insert(attribute);
                }
                return 0;
            }
        }

        #endregion

        #region Push notifications

        public int SavePushNotification(PushNotification message, ApplicationOnDevice applicationOnDevice)
        {
            using (var dbcon = GetConnection())
            {
                var pushNofificationId = dbcon.Insert(message).GetValueOrDefault(0);
                var queuePushNotification = new PushNotificationQueue()
                {
                    PushNotificationId = pushNofificationId,
                    ApplicationOnDeviceId = applicationOnDevice.ApplicationOnDeviceId,
                    //CampaignId = TODO,
                };
                var queuePushNotificationId = dbcon.Insert(queuePushNotification);

                return pushNofificationId;
            }
        }

        /// <summary>
        /// Moves the push notification to archive:
        /// 1. Get notification from queue
        /// 2. Save message to archive
        /// 3. Delete message from queue
        /// 4. Return archive message id
        /// </summary>
        /// <returns>The push notification id to archive.</returns>
        /// <param name="pushNotificationId">Push notification identifier.</param>
        public int MovePushNotificationToArchive(int pushNotificationId, PushNotificationStatus status,
                                           String statusText = "")
        {
            using (var dbcon = GetConnection())
            {
                try
                {
                    var queuePushNotification = dbcon.Query<PushNotificationQueue>(
                                                    "select * from Queue q where q.PushNotificationId = @PushNotificationId",
                                                    new { pushNotificationId }
                                                ).ToList()[0];

                    var archivePushNotification = new PushNotificationArchive(queuePushNotification)
                    {
                        Status = status,
                        StatusDate = DateTime.Now,
                        StatusDescription = statusText
                    };
                    var archivePushNotificationId = dbcon.Insert(archivePushNotification);
                    var deleteCount = dbcon.Delete(queuePushNotification);
                    if (deleteCount != 1)
                    {
                        logger.FatalFormat("Queued notification was not deleted: {0}", queuePushNotification);
                    }

                    return archivePushNotificationId.GetValueOrDefault(0);

                }
                catch (MySqlException ex)
                {
                    logger.Error("Error: something wrong with DB connection pool");
                    throw ex;
                }
            }
        }

        public int SetReadPushNotifications(List<int> pushNotifications)
        {
            using (var dbcon = GetConnection())
            {
                var pushNotificationIds = String.Join(", ", pushNotifications);

                if (pushNotificationIds.Count() == 0)
                    return 0;

                var status = PushNotificationStatus.DELIVER.ToString();

                return dbcon.Execute(
                    "update Archive set Status = @status where PushNotificationId in (@pushNotificationIds)",
                    new { status,  pushNotificationIds }
                );
            }
        }

        public List<PushNotificationArchive> GetCampaignMessages(Campaign campaign)
        {
            using (var dbcon = GetConnection())
            {
                return dbcon.Query<PushNotificationArchive>("select * from Archive where CampaignId = @campaignId",
                    new { campaign.CampaignId }
                ).ToList();
            }
        }

        #endregion
    }
}

